<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181028154932 extends AbstractMigration
{
    /**
     * @param \Doctrine\DBAL\Schema\Schema $schema
     *
     * @return void
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    public function up(Schema $schema) : void
    {
        if(!$schema->hasTable('category')){
            $table = $schema->createTable('category');
            $table->addColumn('id', 'integer', ['autoincrement' => true, 'unsigned' => true]);
            $table->addColumn('title', 'string', ['length' => 255]);
            $table->addColumn('created_at', 'datetime', [
                'default' => $this->connection->getDatabasePlatform()->getCurrentTimestampSQL(),
            ]);
            $table->addUniqueIndex(['title'], 'category_u_title');
            $table->setPrimaryKey(['id'], 'category_u_id');
        }
    }

    /**
     * @param \Doctrine\DBAL\Schema\Schema $schema
     *
     * @return void
     *
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function down(Schema $schema) : void
    {
        if ($schema->hasTable('category')) {
            $schema->dropTable('category');
        }
    }
}
