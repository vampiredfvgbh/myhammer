<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181028160514 extends AbstractMigration
{
    /**
     * @param \Doctrine\DBAL\Schema\Schema $schema
     *
     * @return void
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    public function up(Schema $schema) : void
    {
        if(!$schema->hasTable('job')){
            $table = $schema->createTable('job');
            $table->addColumn('id', 'integer', ['autoincrement' => true, 'unsigned' => true]);
            $table->addColumn('category_id', 'integer', [
                'unsigned' => true,
                'notnull' => false,
                'default' => null,
            ]);
            $table->addColumn('title', 'string', ['length' => 50]);
            $table->addColumn('description', 'text');
            $table->addColumn('zip', 'string', ['length' => 6]);
            $table->addColumn('created_at', 'datetime', [
                'default' => $this->connection->getDatabasePlatform()->getCurrentTimestampSQL(),
            ]);
            $table->addForeignKeyConstraint('category', ['category_id'], ['id'], [
                'onDelete' => 'SET NULL',
                'onUpdate' => 'CASCADE'
            ], 'job_f_category_id');
            $table->setPrimaryKey(['id'], 'job_u_id');
        }
    }

    /**
     * @param \Doctrine\DBAL\Schema\Schema $schema
     *
     * @return void
     *
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function down(Schema $schema) : void
    {
        if ($schema->hasTable('job')) {
            $table = $schema->getTable('job');
            $table->removeForeignKey('job_f_category_id');
            $schema->dropTable('job');
        }
    }
}
