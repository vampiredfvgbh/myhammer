<?php

namespace App\Services;

use App\Entity\Category;
use App\Entity\Job;
use App\Forms\JobForm;
use DateTime;
use Exception;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;

class JobService
{

    /**
     * @var \Symfony\Component\Security\Csrf\CsrfTokenManagerInterface
     */
    protected $csrfTokenManager;

     /**
     * @var \Doctrine\Common\Persistence\ManagerRegistry
     */
    protected $managerRegistry;

    /**
     * @var \Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter
     */
    protected $converter;

    /**
     * @param \Symfony\Component\Security\Csrf\CsrfTokenManagerInterface $csrfTokenManager
     * @param \Doctrine\Common\Persistence\ManagerRegistry $managerRegistry
     * @param \Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter $converter
     */
    public function __construct(
        CsrfTokenManagerInterface $csrfTokenManager,
        ManagerRegistry $managerRegistry,
        CamelCaseToSnakeCaseNameConverter $converter
    ) {
        $this->csrfTokenManager = $csrfTokenManager;
        $this->managerRegistry = $managerRegistry;
        $this->converter = $converter;
    }

    /**
     * @param \App\Entity\Job $job
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @throws \Exception
     *
     * @return bool
     */
    public function add(Job $job, Request $request): bool
    {
        $jobFormName = $this->converter->normalize(JobForm::FORM_NAME);
        $jobForm = $request->request->get($jobFormName);
        if(!is_array($jobForm)){
            throw new Exception('Invalid credentials');
        }

        /*
        if(!isset($registerForm[JobForm::FIELD_TOKEN])) {
            throw new AuthenticationException('Invalid CSRF token.');
        }

        $isValidToken = $this->csrfTokenManager->isTokenValid(
            new CsrfToken(JobForm::FIELD_TOKEN_ID, $jobForm[JobForm::FIELD_TOKEN])
        );

        if (false === $isValidToken) {
            throw new AuthenticationException('Invalid CSRF token.');
        }
        */

        $existedCategory = $this->managerRegistry->getRepository(Category::class)->findOneBy(['id' => $job->getCategoryId()]);
        if (!$existedCategory){
            throw new Exception('Category does not exist');
        }

        $existedJob = $this->managerRegistry->getRepository(Job::class)->findOneBy(['title' => $job->getTitle()]);
        if ($existedJob){
            throw new Exception('Job exists');
        }

        $entityManager = $this->managerRegistry->getManager();
        $dbJob = new Job();
        $dbJob->setTitle($job->getTitle());
        $dbJob->setDescription($job->getDescription());
        $dbJob->setZip($job->getZip());
        $dbJob->setCategoryId($job->getCategoryId());
        $dbJob->setCreatedAt(new DateTime());

        $entityManager->persist($dbJob);
        $entityManager->flush();

        return true;
    }

}