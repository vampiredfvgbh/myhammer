<?php

namespace App\Validators;

use App\Validators\Constraints\NonEqualToOtherField;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Type;

class FormValidator
{

    const VALIDATION_MESSAGE_LENGTH = 'Invalid length';
    const VALIDATION_MESSAGE_EMAIL = 'Invalid Email';
    const VALIDATION_MESSAGE_NUMERIC_TYPE = 'Invalid type: should be numeric';
    const VALIDATION_PATTERN_ADDRESS_NUMBER = '/^(\d+[a-zA-Z]*|\d+[\/\-][1-9]+)$/';
    const VALIDATION_MESSAGE_ADDRESS_NUMBER = 'Invalid address';
    const VALIDATION_MESSAGE_FIELD_MUST_NOT_BE_EMPTY = 'Field must be not empty';
    const VALIDATION_MESSAGE_PASSWORD_CONFIRMATION_NOT_EQUAL = 'Password confirmation is not equal';
    const VALIDATION_MESSAGE_ENTITY_NOT_UNIQUE = 'This already exist. Get another one.';

    /**
     * @return \Symfony\Component\Validator\Constraints\NotBlank
     */
    public function createNotBlankConstraint(): NotBlank
    {
        return new NotBlank([
            'message' => static::VALIDATION_MESSAGE_FIELD_MUST_NOT_BE_EMPTY,
        ]);
    }

    /**
     * @param int $minLength
     *
     * @return \Symfony\Component\Validator\Constraints\Length
     */
    public function createMinLengthConstraint(int $minLength): Length
    {
        return new Length([
            'min' => $minLength,
            'maxMessage' => static::VALIDATION_MESSAGE_LENGTH,
        ]);
    }

    /**
     * @param int $maxLength
     *
     * @return \Symfony\Component\Validator\Constraints\Length
     */
    public function createMaxLengthConstraint(int $maxLength): Length
    {
        return new Length([
            'max' => $maxLength,
            'maxMessage' => static::VALIDATION_MESSAGE_LENGTH,
        ]);
    }

    /**
     * @return \Symfony\Component\Validator\Constraints\Email
     */
    public function createEmailConstraint(): Email
    {
        return new Email([
            'message' => static::VALIDATION_MESSAGE_EMAIL,
        ]);
    }

    /**
     * @return \Symfony\Component\Validator\Constraints\Type
     */
    public function createNumericConstraint(): Type
    {
        return new Type([
            'type' => 'numeric',
            'message' => static::VALIDATION_MESSAGE_NUMERIC_TYPE,
        ]);
    }

    /**
     * @param string $validationGroup
     *
     * @return \Symfony\Component\Validator\Constraints\Regex
     */
    public function createAddressNumberConstraint(string $validationGroup): Regex
    {
        return new Regex([
            'pattern' => static::VALIDATION_PATTERN_ADDRESS_NUMBER,
            'message' => static::VALIDATION_MESSAGE_ADDRESS_NUMBER,
            'groups' => $validationGroup,
        ]);
    }

    /**
     * @param string $nonEqualFieldsName
     *
     * @return \Symfony\Component\Validator\Constraints\Callback
     */
    public function createNonEqualPasswordsValidator(string $nonEqualFieldsName): Callback
    {
        return new Callback([
            'value' => [
                new NonEqualToOtherField($nonEqualFieldsName, static::VALIDATION_MESSAGE_PASSWORD_CONFIRMATION_NOT_EQUAL),
                'validate',
            ],
        ]);
    }

}
