<?php

namespace App\Validators\Constraints;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class NonEqualToOtherField
{

    /**
     * @var string
     */
    protected $otherField;

    /**
     * @var string
     */
    protected $errorMessage;

    /**
     * @param string $otherField
     * @param string $errorMessage
     */
    public function __construct(string $otherField, string $errorMessage)
    {
        $this->otherField = $otherField;
        $this->errorMessage = $errorMessage;
    }

    /**
     * @param string $value
     * @param \Symfony\Component\Validator\Context\ExecutionContextInterface $context
     *
     * @return void
     */
    public function validate(string $value, ExecutionContextInterface $context): void
    {
        $formData = $context->getRoot()->getData();
        $secondField = null;
        if (is_object($formData)) {
            $method = 'get' . ucfirst($this->otherField);
            $secondField = $formData->$method();
        } else {
            $secondField = $formData[$this->otherField];
        }

        if ($secondField === $value) {
            $context->buildViolation($this->errorMessage)->addViolation();
        }
    }

}
