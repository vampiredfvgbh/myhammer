<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Job;
use App\Entity\Zip;
use App\Forms\JobForm;
use App\Services\JobService;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategoryApiController extends AbstractController
{

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @Route("/api/category", name="api_category")
     *
     * @return \Symfony\Component\HttpFoundation\Request
     */
    public function categoryAction(Request $request): Response
    {
        $category = $request->get('category');
        if(!$category){
            return $this->jsonResponse(false, 'No category provided');
        }

        $results = $this->getDoctrine()->getRepository(Category::class)->findBy(['title' => $category]);

        return $this->jsonResponse(true, $results);
    }

}