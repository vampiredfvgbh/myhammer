<?php

namespace App\Controller;

use App\Entity\AuthUser;
use App\Security\FormRegistrar;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController as SymfonyAbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

abstract class AbstractController extends SymfonyAbstractController
{

    /**
     * @var \Symfony\Component\Serializer\Serializer
     */
    protected $serializer;

    /**
     * @var \Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter
     */
    protected $converter;

    /**
     * @param \Symfony\Component\Serializer\Serializer $serializer
     * @param \Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter $converter
     */
    public function __construct(
        Serializer $serializer,
        CamelCaseToSnakeCaseNameConverter $converter
    )
    {
        $this->serializer = $serializer;
        $this->converter = $converter;
    }

    /**
     * @param bool $status
     * @param null|array|string $data
     * @param bool $normalize
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    protected function jsonResponse(bool $status, $data = null, $normalize = true): JsonResponse
    {
        $result = [
            'success' => $status,
            'code' => 200,
        ];

        if ($status) {
            if ($normalize) {
                $result['data'] = $this->serializer->normalize(
                    $data,
                    null,
                    [DateTimeNormalizer::FORMAT_KEY => $this->getParameter('dateTimeFormat')]
                );
            } else {
                $result['data'] = $data;
            }
        } else {
            $result['data'] = ['message' => $data];
        }

        return new JsonResponse($result);
    }

}