<?php

namespace App\Controller;

use App\Entity\Job;
use App\Entity\Zip;
use App\Forms\JobForm;
use App\Services\JobService;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ZipApiController extends AbstractController
{

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @Route("/api/zip", name="api_zip")
     *
     * @return \Symfony\Component\HttpFoundation\Request
     */
    public function zipAction(Request $request): Response
    {
        $zip = $request->get('zip', null);
        if(!$zip){
            return $this->jsonResponse(false, 'No zip provided');
        }
        $results = $this->getDoctrine()->getRepository(Zip::class)->findBy(['zip' => $zip]);

        return $this->jsonResponse(true, $results);
    }

}