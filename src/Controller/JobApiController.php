<?php

namespace App\Controller;

use App\Entity\Job;
use App\Forms\JobForm;
use App\Services\JobService;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class JobApiController extends AbstractController
{

    /**
     * @Route("/api/ping", name="ping")
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function pingAction(Request $request): Response
    {
        return $this->jsonResponse(true, [
            'ping'
        ]);
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \App\Services\JobService $jobService
     *
     * @Route("/api/job", name="api_job")
     *
     * @return \Symfony\Component\HttpFoundation\Request
     */
    public function jobAction(Request $request, JobService $jobService): Response
    {
        $form = $this->createForm(JobForm::class, new Job());
        $form->handleRequest($request);
        $error = null;

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $jobService->add($form->getData(), $request);
            } catch (Exception $e){
                $error = $e->getMessage();
            }
        }

        return $this->jsonResponse($error ? false : true, [
            'form' => $form->createView(),
            'error' => $error,
        ]);
    }

}