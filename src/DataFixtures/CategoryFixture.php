<?php

namespace App\DataFixtures;

use App\Entity\Category;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CategoryFixture extends Fixture
{
    /**
     * @param \Doctrine\Common\Persistence\ObjectManager $manager
     *
     * @return void
     */
    public function load(ObjectManager $manager): void
    {
        $data = [
            ['title' => 'Sonstige Umzugsleistungen',],
            ['title' => 'Abtransport, Entsorgung und Entrümpelung',],
            ['title' => 'Fensterreinigung',],
            ['title' => 'Holzdielen schleifen',],
            ['title' => 'Kellersanierung',],
        ];

        foreach($data as $item){
            $category = new Category();
            $category->setTitle($item['title']);
            $category->setCreatedAt(new DateTime());
            $manager->persist($category);
        }

        $manager->flush();
    }
}