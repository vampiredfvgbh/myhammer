<?php

namespace App\DataFixtures;

use App\Entity\Zip;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ZipFixture extends Fixture
{
    /**
     * @param \Doctrine\Common\Persistence\ObjectManager $manager
     *
     * @return void
     */
    public function load(ObjectManager $manager): void
    {
        $data = [
            ['zip' => '10115', 'city' => 'Berlin',],
            ['zip' => '32457', 'city' => 'Porta Westfalica',],
            ['zip' => '01623', 'city' => 'Lommatzsch',],
            ['zip' => '21521', 'city' => 'Hamburg',],
            ['zip' => '06895', 'city' => 'Bülzig',],
            ['zip' => '01612', 'city' => 'Diesbar-Seußlitz',],
        ];

        foreach($data as $item){
            $zip = new Zip();
            $zip->setZip($item['zip']);
            $zip->setCity($item['city']);
            $zip->setCreatedAt(new DateTime());
            $manager->persist($zip);
        }

        $manager->flush();
    }
}