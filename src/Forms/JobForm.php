<?php

namespace App\Forms;

use App\Entity\AuthUser;
use App\Entity\Job;
use App\Validators\FormValidator;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class JobForm extends AbstractType
{

    const FORM_NAME = 'jobForm';
    const FIELD_TITLE = 'title';
    const FIELD_TITLE_MAXLENGTH = 50;
    const FIELD_CATEGORY_ID = 'category_id';
    const FIELD_DESCRIPTION = 'description';
    const FIELD_ZIP = 'zip';
    const FIELD_ZIP_MAXLENGTH = 6;

    const FIELD_TOKEN = '_token';
    const FIELD_TOKEN_ID = '_csrf_token_id';

    const FIELD_SUBMIT = 'save';
    const FIELD_SUBMIT_LABEL = 'save';

    /**
     * @var \App\Validators\FormValidator
     */
    protected $formValidator;

    /**
     * @param \App\Validators\FormValidator $formValidator
     */
    public function __construct(FormValidator $formValidator)
    {
        $this->formValidator = $formValidator;
    }

    /**
     * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
     *
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Job::class,
            'csrf_protection' => true,
            'csrf_field_name' => static::FIELD_TOKEN,
            'csrf_token_id' => static::FIELD_TOKEN_ID,
        ]);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return static::FORM_NAME;
    }

    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array $options
     *
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $this
            ->addTitleField($builder)
            ->addCategoryIdField($builder)
            ->addDescriptionField($builder)
            ->addZipField($builder)
            ->addSubmitButton($builder);
    }

    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     *
     * @return self
     */
    protected function addTitleField(FormBuilderInterface $builder): self
    {
        $builder->add(static::FIELD_TITLE, TextType::class, [
            'required' => true,
            'constraints' => [
                $this->formValidator->createMaxLengthConstraint(static::FIELD_TITLE_MAXLENGTH),
                $this->formValidator->createNotBlankConstraint(),
            ],
        ]);

        return $this;
    }

    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     *
     * @return self
     */
    protected function addCategoryIdField(FormBuilderInterface $builder): self
    {
        $builder->add(static::FIELD_CATEGORY_ID, TextType::class, [
            'required' => true,
            'constraints' => [
                $this->formValidator->createNotBlankConstraint(),
                $this->formValidator->createNumericConstraint(),
            ],
        ]);

        return $this;
    }

    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     *
     * @return self
     */
    protected function addDescriptionField(FormBuilderInterface $builder): self
    {
        $builder->add(static::FIELD_DESCRIPTION, TextareaType::class, [
            'required' => true,
            'constraints' => [
                $this->formValidator->createNotBlankConstraint(),
            ],
        ]);

        return $this;
    }

    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     *
     * @return self
     */
    protected function addZipField(FormBuilderInterface $builder): self
    {
        $builder->add(static::FIELD_ZIP, TextType::class, [
            'required' => true,
            'constraints' => [
                $this->formValidator->createMaxLengthConstraint(static::FIELD_ZIP_MAXLENGTH),
                $this->formValidator->createNotBlankConstraint(),
                $this->formValidator->createNumericConstraint(),
            ],
        ]);

        return $this;
    }

    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     *
     * @return self
     */
    protected function addSubmitButton(FormBuilderInterface $builder): self
    {
        $builder->add(static::FIELD_SUBMIT, SubmitType::class, [
            'label' => static::FIELD_SUBMIT_LABEL,
        ]);

        return $this;
    }

}